const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
//compile scss into css
function style() {
    return gulp.src('assets/scss/**/*.scss')
    .pipe(sass().on('error',sass.logError))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.stream());
}
function watch() {
    browserSync.init({
        proxy: 'https://loc.codafication.com',
        open: 'external',
        host: 'localhost',
    });
    gulp.watch(['assets/scss/**/*.scss', 'templates/**/*.scss'], style);
    gulp.watch(['assets/js/**/*.js']).on('change',browserSync.reload);
    gulp.watch('./*.php').on('change',browserSync.reload);
}
exports.style = style;
exports.watch = watch;
