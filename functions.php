<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementor
 */
// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');
require_once( 'inc/menu-walker-elementor.php' );

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'THEME_VERSION', '1.0.0' );
// define( 'WP_SCSS_ALWAYS_RECOMPILE', true );

require_once 'lib/functions.php';

$includes = [
	'lib/setup.php',
	'lib/elementor.php',
	'shortcodes/reading-time.php',
	'shortcodes/blogs-filter.php',
	'shortcodes/template-header.php',
	'shortcodes/tooltip.php',
	'shortcodes/post-source.php',
];

if ( is_woocommerce_activated() ) {
	$includes[] = 'lib/woocommerce.php';
}

foreach ( $includes as $file ) {
	require_once $file;
}

register_nav_menus( array(
    'primary' => esc_html__( 'Primary Menu', 'HelloElementor' ),
    'footer-nav' => "Footer Menu"
) );

define('WP_MEMORY_LIMIT', '256M');


// wp_register_script( 'magnet-mouse', 'https://cdn.jsdelivr.net/gh/fluffy-factory/magnet-mouse@latest/lib/magnet-mouse.min.js', null, null, true );
// wp_enqueue_script('magnet-mouse');
// <script src="https://cdn.jsdelivr.net/gh/fluffy-factory/magnet-mouse@latest/lib/magnet-mouse.min.js"></script>
