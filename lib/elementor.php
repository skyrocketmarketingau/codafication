<?php

// Load the theme's custom Widgets so that they appear in the Elementor element panel.
add_action( 'elementor/widgets/widgets_registered', 'register_elementor_widgets' );
function register_elementor_widgets() {
	// We check if the Elementor plugin has been installed / activated.
	if ( defined( 'ELEMENTOR_PATH' ) && class_exists('Elementor\Widget_Base') ) {
		require_once 'elements/custom-slider/index.php';
		require_once 'elements/2column-slider/index.php';
		require_once 'elements/review-box/index.php';
 	}
}


add_action( 'elementor/frontend/after_enqueue_styles', 'theme_after_enqueue_styles' );
function theme_after_enqueue_styles() {

	// Don't remove it in the backend
	if ( is_admin() || current_user_can( 'manage_options' ) ) {
		return;
	}

	wp_dequeue_style( 'elementor-icons' );
	wp_dequeue_style( 'font-awesome' );
	wp_dequeue_style( 'elementor-animations' );
	// wp_dequeue_style( 'elementor-frontend' );
}

add_action( 'elementor/frontend/after_register_scripts', 'theme_after_register_scripts' );
function theme_after_register_scripts() {

	// Don't remove it in the backend
	if ( is_admin() || current_user_can( 'manage_options' ) ) {
		return;
	}

	// wp_deregister_script( 'elementor-waypoints' );
	// wp_deregister_script( 'imagesloaded' );
	// wp_deregister_script( 'jquery-numerator' );
	// wp_deregister_script( 'jquery-swiper' );
	wp_deregister_script( 'jquery-slick' );
	// wp_deregister_script( 'elementor-dialog' );
	// wp_deregister_script( 'elementor-frontend' );
}
