<?php
/**
 * theme theme setup options
 *
 * @package theme
 * @since theme 1.0
 */

if ( ! isset( $content_width ) ) {
	$content_width = 998;
}

/**
 * Theme setup
 */
add_action( 'after_setup_theme', 'theme_setup' );
function theme_setup() {

	remove_action( 'wp_head', 'wp_generator' );
	add_theme_support( 'automatic-feed-links' );
	// add_theme_support( 'align-wide' );

	// Enable plugins to manage the document title
	// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
	add_theme_support( 'title-tag' );

	add_theme_support(
		'custom-logo',
		array(
			'width'       => 200,
			'height'      => 80,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		)
	);

	// Register wp_nav_menu() menus
	// http://codex.wordpress.org/Function_Reference/register_nav_menus
	register_nav_menus(
		[
			'primary_navigation' => __( 'Primary Navigation', 'theme' ),
		]
	);

	// Enable post thumbnails
	// http://codex.wordpress.org/Post_Thumbnails
	// http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
	// http://codex.wordpress.org/Function_Reference/add_image_size
	add_theme_support( 'post-thumbnails' );

	// Enable HTML5 markup support
	// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );
}

/**
 * Register sidebars
 */
add_action( 'widgets_init', 'theme_widgets' );
function theme_widgets() {

	register_sidebar( array(
		'name'          => 'Sidebar',
		'id'            => 'sidebar-1',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget__title h5">',
		'after_title'   => '</div>',
	) );

	register_sidebar( array(
		'name'          => 'Footer 1',
		'id'            => 'footer-1',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget__title h5">',
		'after_title'   => '</div>',
	) );

}

/**
 * Theme assets
 */
add_action( 'wp_enqueue_scripts', 'theme_assets', 100 );
function theme_assets() {
	wp_enqueue_style( 'theme-styles', get_template_directory_uri() . '/assets/css/style.css', false );
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.css', array(), 20141119 );

	wp_enqueue_script( 'theme-scripts', get_template_directory_uri() . '/assets/js/scripts.js', [ 'jquery' ], '1.0', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '5.1.0', true );	

	wp_enqueue_script( 'magnet-mouse', get_template_directory_uri() . '/assets/js/magnet-mouse.min.js', array('jquery'), '1.0.0', true );	

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
}

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page();
}
