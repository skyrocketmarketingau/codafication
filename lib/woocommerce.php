<?php

add_theme_support( 'woocommerce' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

$includes = [
	'woocommerce/assets.php',
	'woocommerce/checkout.php',
];

foreach ( $includes as $file ) {
	require_once $file;
}

