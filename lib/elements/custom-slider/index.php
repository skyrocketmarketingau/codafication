<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Core\Schemes;

class Elementor_Widget_Custom_Slider extends \Elementor\Widget_Base {

	public function __construct( $data = [], $args = null ) {
		parent::__construct( $data, $args );

		wp_enqueue_script( 'custom-slider', get_template_directory_uri() . '/lib/elements/custom-slider/scripts.js', [ 'swiper' ], '1.0.0', true );
		wp_enqueue_style( 'custom-slider', get_template_directory_uri() . '/lib/elements/custom-slider/style.css', '1.0.0' );
	}

	public function get_script_depends() {
		return [ 'custom-slider' ];
	}

	public function get_style_depends() {
		return [ 'custom-slider' ];
	}

	public function get_name() {
		return 'custom-slider';
	}

	public function get_title() {
		return __( 'Custom Slider', 'elementor-toolbox' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'elementor-toolbox' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$repeater = new \Elementor\Repeater();
		
		$repeater->add_control(
			'feature_image', [
				'label' => __( 'Choose Feature Image', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				]
			]
		);
		
		$repeater->add_control(
			'feature_title', [
				'label' => __( 'Feature Title', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'show_label' => true,
			]
		);
		
		$repeater->add_control(
			'feature_content', [
				'label' => __( 'Feature Content', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'show_label' => true,
			]
		);
		
		$repeater->add_control(
			'feature_button_url', [
				'label' => __( 'Feature Button URL', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'show_label' => true,
			]
		);
		
		$repeater->add_control(
			'feature_button_label', [
				'label' => __( 'Feature Button Label', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'show_label' => true,
			]
		);
		

		$repeater->add_control(
			'story_quote', [
				'label' => __( 'Quote', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'show_label' => true,
			]
		);

		$repeater->add_control(
			'story_name', [
				'label' => __( 'Name', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'story_position', [
				'label' => __( 'Position', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'story_photo', [
				'label' => __( 'Choose Image', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				]
			]
		);

		$repeater->add_control(
			'story_media', [
				'label' => __( 'Media', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::URL
			]
		);

		$this->add_control(
			'stories',
			[
				'label' => __( 'Slider', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'story_quote' => __( '“Nec enim parcam s iis at nisi impedit torquent fuga si ante numerum porta rudera mus potens satisfacti. Crimen y expeditionem, calcat fuga ut invidiae.”', 'elementor-toolbox' ),
						'story_name' => __( 'Hostem Eius', 'elementor-toolbox' ),
						'story_position' => __( 'Quaeque Pericula', 'elementor-toolbox' ),
						'feature_title' => __( 'Generis quos', 'elementor-toolbox' ),
						'feature_content' => __( 'Servire bcatissimae illo ac eros, domine eu culpa hac piscis', 'elementor-toolbox' ),
						'feature_button_label' => __( 'Button Text', 'elementor-toolbox' ),
					],
				],
				'title_field' => '{{{ story_name }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'background',
			[
				'label' => __( 'Background Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .slider__content' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'border_color',
			[
				'label' => __( 'Border Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .slider__content' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_style',
			[
				'label' => __( 'Button Style', 'elementor' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'default' => __( 'Default', 'elementor' ),
					'btn-outline-primary' => __( 'Outline', 'elementor' ),
				],
				'default' => 'default',
			]
		);

		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		if ( $settings['stories'] ) {
			?>
			<audio id="sliderAudio" src="" preload="auto"></audio>
			<div class="slider__swiper custom-slider">
				<div class="swiper-wrapper">
				<?php
				foreach ( $settings['stories'] as $item ) {
					?>
					<div class="slider swiper-slide">
						<div class="slider__content">
							<div class="slider__body">
								
								<div class="slider__feature--image">
										<img src="<?php echo $item['feature_image']['url']; ?>">
								</div>
								
								<div class="slider__feature--title">
											<?php echo $item['feature_title']; ?>
								</div>
								
								<div class="slider__feature--content">
											<?php echo $item['feature_content']; ?>
								</div>
								
								<?php if ($item['feature_button_url']): ?>
									<div class="elementor-element slider__feature--button <?php echo $settings['button_style']; ?>">
										<a class="elementor-button" href="<?php echo $item['feature_button_url']; ?>"><?php echo $item['feature_button_label']; ?></a>
									</div>
								<?php endif; ?>
								
								<div class="slider__divider"></div>
								
								<div class="slider__quote">
									<?php echo $item['story_quote']; ?>
								</div>
								<?php if ( $item['story_name'] || $item['story_position'] ) { ?>
								<div class="slider__details">
									<div class="slider__image">
										<img src="<?php echo $item['story_photo']['url']; ?>">
									</div>
									<div class="slider__credentials">
										<div class="slider__name">
											<?php echo $item['story_name']; ?>
										</div>
										<div class="slider__position">
											<?php echo $item['story_position']; ?>
										</div>
									</div>
									
								</div>
								<?php } ?>
								<?php if( $item['story_media']['url'] ) { ?>
									<div class="slider__audio elementor-element <?php echo $settings['button_style']; ?>">
										<a class="elementor-button" href="<?php echo $item['story_media']['url']; ?>">Play audio</a>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php
				}
				?>
				</div>
				<div class="swiper-button-next"></div>
				<div class="swiper-button-prev"></div>
			</div>
			<script>
			(function($) {
				var audio = $('#sliderAudio').get(0);

				$('body').on('click', '.slider__swiper a[href*=".m"]', function(e) {
					e.preventDefault();

					var audioSrc   = $(this).attr('href');
					var buttonText = $(this).text();

					$('#sliderAudio').attr('src', audioSrc);

					if ( buttonText == 'Play audio' ) {
						buttonText = 'Stop audio';
						audio.play();
					} else {
						buttonText = 'Play audio';
						audio.pause();
						audio.currentTime = 0;
					}

					$(this).text(buttonText);
				});

				audio.onended = function() {
					$('.slider__swiper a[href*=".m"]').each(function() {
						$(this).text('Play audio');
					});
				}
			})(jQuery);
			</script>
			<?php
		}
	}

	protected function _content_template() {
		?>
		<# if ( settings.stories.length ) { #>
			<audio id="sliderAudio" src="" preload="auto"></audio>
			<div class="slider__swiper custom-slider">
				<div class="swiper-wrapper">
			<# _.each( settings.stories, function( item ) { #>
				<div class="slider swiper-slide" style="opacity: 1">
					<div class="slider__content">
						<div class="slider__body">
							
							<div class="slider__feature--image">
								<img src="{{{ item.feature_image.url }}}">
							</div>
							
							<div class="slider__feature--title">
								{{{ item.feature_title }}}
							</div>
							
							<div class="slider__feature--content">
								{{{ item.feature_content }}}
							</div>
							
							<# if ( item.feature_button_url.length ) { #>
								<div class="elementor-element slider__feature--button {{{ item.button_style }}} ?>">
									<a class="btn btn-primary" href="{{{ item.feature_button_url }}}">{{{ item.feature_button_label }}}</a>
								</div>
							<# } #>
								
							<div class="slider__divider"></div>
							
							<div class="slider__quote">
								{{{ item.story_quote }}}
							</div>
								
							<div class="slider__details">
								<div class="slider__image">
									<img src="{{{ item.story_photo.url }}}">
								</div>
								<div class="slider__credentials">
									<div class="slider__name">
										{{{ item.story_name }}}
									</div>
									<div class="slider__position">
										{{{ item.story_position }}}
									</div>
								</div>
							</div>
								
							<# if ( item.story_media.url.length ) { #>
							<div class="slider__audio elementor-element {{{ item.button_style }}}">
								<a class="elementor-button" href="{{{ item.story_media.url }}}">Play audio</a>
							</div>
							<# } #>
						</div>
					</div>
				</div>
			<# }); #>
				</div>
				<div class="swiper-button-next"></div>
				<div class="swiper-button-prev"></div>
			</div>
		<# } #>
		<?php
	}
}

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor_Widget_Custom_Slider() );
