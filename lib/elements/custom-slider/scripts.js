(function($) {
  $(function() {
    if ( $('.slider__swiper .swiper-wrapper').children().length > 1 ) {
      new Swiper ('.slider__swiper', {
          speed: 600,
          pagination: {
              el: '.swiper-pagination'
          },
          navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
          }
      });
    }
  });
})(jQuery);
