<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Core\Schemes;

class Elementor_Widget_Review_Box extends \Elementor\Widget_Base {

	public function __construct( $data = [], $args = null ) {
		parent::__construct( $data, $args );
		
		wp_enqueue_script( 'review-box', get_template_directory_uri() . '/lib/elements/review-box/scripts.js', [ 'swiper' ], '1.0.0', true );
		wp_enqueue_style( 'review-box', get_template_directory_uri() . '/lib/elements/review-box/style.css', '1.0.0' );	
	}

	public function get_script_depends() {
		return [ 'review-box' ];
	}

	public function get_style_depends() {
		return [ 'review-box' ];
	}

	public function get_name() {
		return 'review-box';
	}

	public function get_title() {
		return __( 'Review Box', 'primary' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'primary' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'stars',
			[
				'label'   => __( 'Stars', 'primary' ),
				'type'    => \Elementor\Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 5,
				'step'    => 1,
				'default' => 4,
			]
		);

		$this->add_control(
			'content',
			[
				'label'   => __( 'Content', 'primary' ),
				'type'    => \Elementor\Controls_Manager::TEXTAREA,
				'default' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at risus sit amet nibh scelerisque mattis. Vestibulum dignissim nunc nulla.',
			]
		);

		$this->add_control(
			'name',
			[
				'label'   => __( 'Name', 'primary' ),
				'type'    => \Elementor\Controls_Manager::TEXT,
				'default' => 'Name',
			]
		);

		$this->add_control(
			'date',
			[
				'label'   => __( 'Date', 'primary' ),
				'type'    => \Elementor\Controls_Manager::DATE_TIME,
				'default' => '2020-10-09',
			]
		);

		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		?>
		<div class="review-box">
			<div class="review-box__stars stars stars--<?php echo $settings['stars']; ?>">
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
			</div>
			<div class="review-box__content">
				<?php echo $settings['content']; ?>
			</div>
			<div class="review-box__author">
				<span class="review-box__name">
					<?php echo $settings['name']; ?>
				</span>
				<span class="review-box__date">
					<?php echo date( 'F j, Y', strtotime( $settings['date'] ) ); ?>
				</span>
			</div>
		</div>
		<?php
	}

	protected function _content_template() {
		?>
		<div class="review-box">
			<div class="review-box__stars stars stars--{{ settings.stars }}">
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
				<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15 22.6136L21.2113 26.3624C22.1913 26.9536 23.4 26.0749 23.14 24.9611L21.4913 17.8949L26.98 13.1399C27.845 12.3911 27.3826 10.9699 26.2425 10.8736L19.0176 10.2611L16.1913 3.59239C15.7451 2.54114 14.255 2.54114 13.8088 3.59239L10.9825 10.2611L3.75755 10.8736C2.61755 10.9699 2.15505 12.3911 3.02005 13.1399L8.5088 17.8949L6.86005 24.9611C6.60005 26.0749 7.8088 26.9536 8.7888 26.3624L15 22.6136Z" fill="#EBEBED"/>
				</svg>
			</div>
			<div class="review-box__content">
				{{ settings.content }}
			</div>
			<div class="review-box__author">
				<span class="review-box__name">
					{{ settings.name }}
				</span>
				<span class="review-box__date">
					{{ settings.date }}
				</span>
			</div>
		</div>
		<?php
	}
}

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor_Widget_Review_Box() );
