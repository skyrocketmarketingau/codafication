<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Core\Schemes;

class Elementor_Widget_Slider extends \Elementor\Widget_Base {

	public function __construct( $data = [], $args = null ) {
		parent::__construct( $data, $args );

		wp_enqueue_script( '2column-slider', get_template_directory_uri() . '/lib/elements/2column-slider/scripts.js', [ 'swiper' ], '1.0.0', true );
		wp_enqueue_style( '2column-slider', get_template_directory_uri() . '/lib/elements/2column-slider/style.css', '1.0.0' );
	}

	public function get_script_depends() {
		return [ '2column-slider' ];
	}

	public function get_style_depends() {
		return [ '2column-slider' ];
	}

	public function get_name() {
		return '2column-slider';
	}

	public function get_title() {
		return __( '2 Column Slider', 'elementor-toolbox' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'elementor-toolbox' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$repeater = new \Elementor\Repeater();
		
		$repeater->add_control(
			'feature_icon', [
				'label' => __( 'Choose Feature Icon', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				]
			]
		);

		$repeater->add_control(
			'feature_image', [
				'label' => __( 'Choose Feature Image', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				]
			]
		);
		
		$repeater->add_control(
			'feature_title', [
				'label' => __( 'Feature Title', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'show_label' => true,
			]
		);
		
		$repeater->add_control(
			'feature_content', [
				'label' => __( 'Feature Content', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'show_label' => true,
			]
		);

		$repeater->add_control(
			'feature_button_url', [
				'label' => __( 'Feature Button URL', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'show_label' => true,
			]
		);

		$this->add_control(
			'stories',
			[
				'label' => __( 'Stories', 'elementor-toolbox' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'feature_title' => __( 'Feature name', 'elementor-toolbox' ),
						'feature_content' => __( 'Feature description will go here, should be short and direct', 'elementor-toolbox' ),
						'feature_button_url' => __( '#', 'elementor-toolbox' ),
					],
				],
				'title_field' => '{{{ feature_title }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'background',
			[
				'label' => __( 'Background Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cs__content' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'border_color',
			[
				'label' => __( 'Border Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cs__content' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_style',
			[
				'label' => __( 'Button Style', 'elementor' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'default' => __( 'Default', 'elementor' ),
					'btn-outline-primary' => __( 'Outline', 'elementor' ),
				],
				'default' => 'default',
			]
		);

		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		if ( $settings['stories'] ) {
			?>
			<div class="cs__swiper hiw-slider">
				<div class="swiper-wrapper">
				<?php
				foreach ( $settings['stories'] as $item ) {
					?>
					<div class="cs swiper-slide">
						<div class="cs__content">
							<div class="cs__body">
								
								<div class="cs__feature--content">
									<img src="<?php echo $item['feature_icon']['url']; ?>" class="cs__feature--icon">
									<h4><?php echo $item['feature_title']; ?></h4>
									<div><?php echo $item['feature_content']; ?></div>
									<?php if ($item['feature_button_url']): ?>
										<a href="<?php echo $item['feature_button_url']; ?>" class="button">See how</a>
									<?php endif; ?>
								</div>

								<div class="cs__feature--image">
										<img src="<?php echo $item['feature_image']['url']; ?>">
								</div>
							</div>
						</div>
					</div>
					<?php
				}
				?>
				</div>
				<div class="swiper-pagination"></div>
			</div>
			<?php
		}
	}

	protected function _content_template() {
		?>
		<# if ( settings.stories.length ) { #>
			<div class="cs__swiper hiw-slider">
				<div class="swiper-wrapper">
			<# _.each( settings.stories, function( item ) { #>
				<div class="cs swiper-slide" style="opacity: 1">
					<div class="cs__content">

						<div class="cs__body">
								
							<div class="cs__feature--content">
								<img src="{{{ item.feature_icon.url }}}" class="cs__feature--icon">
								<h4>{{{ item.feature_title }}}</h4>
								<div>{{{ item.feature_content }}}</div>
								<# if ( item.feature_button_url.length ) { #>
									<a href="{{{ item.feature_button_url }}}" class="button">See how</a>
								<# } #>
							</div>

							<div class="cs__feature--image">
									<img src="{{{ item.feature_image.url }}}">
							</div>
						</div>
					</div>
				</div>
			<# }); #>
				</div>
				<div class="swiper-pagination"></div>
			</div>
		<# } #>
		<?php
	}
}

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor_Widget_Slider() );
