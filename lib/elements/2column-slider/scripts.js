(function($) {
  $(function() {
    if ( $('.cs__swiper .swiper-wrapper').children().length > 1 ) {
      new Swiper ('.cs__swiper', {
          speed: 600,
          centeredSlides: true,
          spaceBetween: 20,
          slidesPerView: 'auto',
          pagination: {
              el: '.swiper-pagination',
              type: 'bullets',
              clickable: true
          }
      });
    }
  });
})(jQuery);
