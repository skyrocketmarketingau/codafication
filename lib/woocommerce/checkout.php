<?php

add_action( 'template_redirect', 'plugin_is_page' );

function plugin_is_page() {
    global $woocommerce;

    $checkout_id = url_to_postid( wc_get_cart_url() );
    if ( is_page( $checkout_id ) ) {
        wp_enqueue_script( 'woocommerce-checkout', get_template_directory_uri() . '/assets/js/woocommerce-checkout.js', [], '1.0.0', true );
    }
}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
add_action( 'woocommerce_before_checkout_billing_form', 'woocommerce_checkout_login_form', 10 );

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
add_action( 'woocommerce_review_order_after_cart_contents', function() {
    echo '<tr>';
    echo '<td class="cart-coupon" colspan="2">';
    woocommerce_checkout_coupon_form();
    echo '</td>';
    echo '</tr>';
});

// REORDERING CHECKOUT BILLING FIELDS (WOOCOMMERCE 3+)
add_filter( 'woocommerce_checkout_fields', 'reordering_checkout_fields', 15, 1 );
function reordering_checkout_fields( $fields ) {

    ## ---- 1. REORDERING BILLING FIELDS ---- ##

    // Set the order of the fields
    $billing_order = array(
        'billing_email',
        'billing_first_name',
        'billing_last_name',
        'billing_company',
        'billing_address_1',
        'billing_address_2',
        'billing_city',
        'billing_country',
        'billing_state',
        'billing_postcode',
        'billing_phone',
    );
    $shipping_order = array(
        'shipping_first_name',
        'shipping_last_name',
        'shipping_company',
        'shipping_address_1',
        'shipping_address_2',
        'shipping_city',
        'shipping_country',
        'shipping_state',
        'shipping_postcode',
        'shipping_phone',
    );

    $count = 0;
    $priority = 10;

    // Updating the 'priority' argument
    foreach( $billing_order as $field_name ) {
        $count++;
        $fields['billing'][$field_name]['priority'] = $count * $priority;
    }

    $count = 0;
    $priority = 10;

    foreach( $shipping_order as $field_name ) {
        $count++;
        $fields['shipping'][$field_name]['priority'] = $count * $priority;
    }

    ## ---- 2. CHANGING SOME CLASSES FOR BILLING FIELDS ---- ##

    $fields['billing']['billing_email']['placeholder']      = 'Email';
    $fields['billing']['billing_first_name']['placeholder'] = 'First name';
    $fields['billing']['billing_last_name']['placeholder']  = 'Last name';
    $fields['billing']['billing_company']['placeholder']    = 'Company name (optional)';
    $fields['billing']['billing_city']['placeholder']       = 'Suburb';
    $fields['billing']['billing_postcode']['placeholder']   = 'Postcode';
    $fields['billing']['billing_phone']['placeholder']      = 'Phone';

    $fields['shipping']['shipping_first_name']['placeholder'] = 'First name';
    $fields['shipping']['shipping_last_name']['placeholder']  = 'Last name';
    $fields['shipping']['shipping_company']['placeholder']    = 'Company name (optional)';
    $fields['shipping']['shipping_city']['placeholder']       = 'Suburb';
    $fields['shipping']['shipping_postcode']['placeholder']   = 'Postcode';
    $fields['shipping']['shipping_phone']['placeholder']      = 'Phone';

    $fields['billing']['billing_email']['class']      = array('form-row-wide');
    $fields['billing']['billing_first_name']['class'] = array('form-row-first');
    $fields['billing']['billing_last_name']['class']  = array('form-row-last');
    $fields['billing']['billing_company']['class']    = array('form-row-wide');
    $fields['billing']['billing_address_1']['class']  = array('form-row-wide');
    $fields['billing']['billing_address_2']['class']  = array('form-row-wide');
    $fields['billing']['billing_city']['class']       = array('form-row-wide');
    $fields['billing']['billing_country']['class']    = array('form-row-first-third');
    $fields['billing']['billing_state']['class']      = array('form-row-middle-third');
    $fields['billing']['billing_postcode']['class']   = array('form-row-last-third');
    $fields['billing']['billing_phone']['class']      = array('form-row-wide');

    $fields['shipping']['shipping_email']['class']      = array('form-row-wide');
    $fields['shipping']['shipping_first_name']['class'] = array('form-row-first');
    $fields['shipping']['shipping_last_name']['class']  = array('form-row-last');
    $fields['shipping']['shipping_company']['class']    = array('form-row-wide');
    $fields['shipping']['shipping_address_1']['class']  = array('form-row-wide');
    $fields['shipping']['shipping_address_2']['class']  = array('form-row-wide');
    $fields['shipping']['shipping_city']['class']       = array('form-row-wide');
    $fields['shipping']['shipping_country']['class']    = array('form-row-first-third');
    $fields['shipping']['shipping_state']['class']      = array('form-row-middle-third');
    $fields['shipping']['shipping_postcode']['class']   = array('form-row-last-third');
    $fields['shipping']['shipping_phone']['class']      = array('form-row-wide');

    ## ---- RETURN THE BILLING FIELDS CUSTOMIZED ---- ##

    return $fields;
}

add_action( 'woocommerce_form_field_text', 'reigel_custom_heading', 10, 2 );
function reigel_custom_heading( $field, $key ) {
    if ( is_checkout() && ( $key == 'billing_first_name') ) {
        if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) {
            $field = '<div class="form-row form-row-wide"><h3>' . __('Billing &amp; shipping address') . '</h3></div>' . $field;
        } else {
            $field = '<div class="form-row form-row-wide"><h3>' . __('Billing address') . '</h3></div>' . $field;
        }
    }
    return $field;
}

// Hook in
add_filter( 'woocommerce_default_address_fields', 'custom_override_default_address_fields' );

// Our hooked in function - $address_fields is passed via the filter!
function custom_override_default_address_fields( $address_fields ) {
    $address_fields['address_1']['placeholder'] = 'Address (No PO Boxes)';
    $address_fields['address_2']['placeholder'] = 'Apartment, suite, etc (optional)';

    return $address_fields;
}
