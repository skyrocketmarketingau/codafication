<?php

add_shortcode( 'post_source', 'post_source' );
function post_source() {
	global $post;

	// Shortcode Attributes
	$atts = shortcode_atts( array(
	), $atts, 'post_source' );

	ob_start();

	$source = get_post_meta( $post->ID, '_links_to', true );
	$parse = parse_url( $source );

	if ( $source && $parse['host'] ) {
		?>
		<div class="link">
			<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M30 31.6667H10C9.08333 31.6667 8.33333 30.9167 8.33333 30V10C8.33333 9.08333 9.08333 8.33333 10 8.33333H18.3333C19.25 8.33333 20 7.58333 20 6.66667C20 5.75 19.25 5 18.3333 5H8.33333C6.48333 5 5 6.5 5 8.33333V31.6667C5 33.5 6.5 35 8.33333 35H31.6667C33.5 35 35 33.5 35 31.6667V21.6667C35 20.75 34.25 20 33.3333 20C32.4167 20 31.6667 20.75 31.6667 21.6667V30C31.6667 30.9167 30.9167 31.6667 30 31.6667ZM23.3333 6.66667C23.3333 7.58333 24.0833 8.33333 25 8.33333H29.3167L14.1 23.55C13.45 24.2 13.45 25.25 14.1 25.9C14.75 26.55 15.8 26.55 16.45 25.9L31.6667 10.6833V15C31.6667 15.9167 32.4167 16.6667 33.3333 16.6667C34.25 16.6667 35 15.9167 35 15V6.66667C35 5.75 34.25 5 33.3333 5H25C24.0833 5 23.3333 5.75 23.3333 6.66667Z" fill="#26BCB4"/>
			</svg>
			<?php echo str_replace( 'www.', '', $parse['host'] ); ?>
		</div>
		<?php
	} else {
		$avatar   = get_avatar( get_the_author_meta( 'user_email' ), 40 );
		$name     = get_the_author_meta( 'display_name' );
		$position = get_field( 'position', 'user_' . $post->post_author );
		?>
		<div class="author">
			<?php echo $avatar ; ?>
			<div class="author__details">
				<div class="author__name">
					<?php echo $name; ?>
				</div>
				<div class="author__position">
					<?php echo $position; ?>
				</div>
			</div>
		</div>
		<?php
	}

	$output = ob_get_clean();
	return $output;
}
