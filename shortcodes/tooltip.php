<?php
add_shortcode( 'tooltip', 'shortcode_tooltip' );
function shortcode_tooltip( $atts ) {
	// Shortcode Attributes
	$atts = shortcode_atts( array(
		'content' => ''
	), $atts, 'tooltip' );

	ob_start();
	?>

	<button type="button" class="tooltip-help" style="background: none" data-toggle="tooltip" data-placement="right" title="<?php echo $atts['content']; ?>">
		<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="10" cy="10" r="9" stroke="#949BAA" stroke-width="2"/>
			<path d="M8.991 12.194c.004-.385.036-.717.095-.996.064-.28.167-.531.311-.756.144-.224.339-.448.584-.672.195-.174.366-.341.514-.502.153-.16.273-.328.362-.501.089-.174.134-.369.134-.584 0-.364-.09-.641-.267-.832-.178-.19-.436-.286-.775-.286-.279 0-.53.087-.755.26-.224.174-.338.456-.343.845H7.1c.009-.576.138-1.05.388-1.422a2.283 2.283 0 011.022-.838c.431-.186.911-.28 1.44-.28.876 0 1.56.215 2.05.642.492.427.737 1.039.737 1.834 0 .373-.074.703-.222.99a3.455 3.455 0 01-.571.813c-.233.25-.49.504-.768.762a1.659 1.659 0 00-.483.68c-.08.236-.125.517-.133.843H8.99zm-.159 1.955c0-.262.087-.482.26-.66.178-.182.417-.273.718-.273.304 0 .544.091.717.273a.896.896 0 01.267.66.897.897 0 01-.267.66c-.173.178-.412.267-.717.267-.3 0-.54-.089-.717-.266a.908.908 0 01-.26-.66z" fill="#949BAA"/>
		</svg>
	</button>

	<?php
	$output = ob_get_clean();
	return $output;
}
