<?php
add_shortcode( 'template_header', 'shortcode_template_header' );
function shortcode_template_header( $atts ) {
	// Shortcode Attributes
	$atts = shortcode_atts( array(
		'style' => 'default'
	), $atts, 'template_header' );
	ob_start();
	?>

	<div class="e-header header--<?php echo $atts['style']; ?>">
		<?php get_template_part( 'template-parts/header', 'elementor' ); ?>
	</div>

	<?php
	$output = ob_get_clean();
	return $output;
}
