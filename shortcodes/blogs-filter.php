<?php

add_shortcode( 'blogs_filter', 'blogs_filter_shortcode' );
function blogs_filter_shortcode() {

	// Shortcode Attributes
	$atts = shortcode_atts( array(
		'show_count'    => '0',
		'hierarchical'  => '0',
		'orderby'       => '',
	), $atts, 'blogs_filter' );

	ob_start();

	$args = array(
		'show_option_all'  => 'Select topic',
		'show_count'       => $atts['show_count'],
		'hierarchical'     => $atts['hierarchical'],
		'value_field'      => 'slug',
		'class'            => 'select',
		'selected'         => $kat = get_query_var( 'cat' ),
		'orderby'          => ( isset($atts['orderby']) && empty($atts['orderby']) ? $atts['orderby'] : 'order' ),
	);

	$term = get_term_by( 'id', get_query_var( 'cat' ), 'category' );
	if ( $term ) {
		$args['selected'] = $term->slug;
	}
	?>

	<div class="blogs-filter">
		<div class="blogs-filter__select">
			<div class="blogs-filter__select-title">Show only</div>
			<div class="blogs-filter__select-form select-wrapper form--white">
				<?php wp_dropdown_categories( $args ); ?>
			</div>
			<script type='text/javascript'>
				jQuery(function($) {
					$('#cat').change(function() {
						if( $(this).val() !== '' ) {
							location.href = '<?php echo home_url(); ?>/in-the-media/category/'+$(this).val();
						}
					});
				});
			</script>
		</div>

		<form class="blogs-filter__form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M15.4996 14H14.7096L14.4296 13.73C15.6296 12.33 16.2496 10.42 15.9096 8.39002C15.4396 5.61002 13.1196 3.39002 10.3196 3.05002C6.08965 2.53002 2.52965 6.09001 3.04965 10.32C3.38965 13.12 5.60965 15.44 8.38965 15.91C10.4196 16.25 12.3296 15.63 13.7296 14.43L13.9996 14.71V15.5L18.2496 19.75C18.6596 20.16 19.3296 20.16 19.7396 19.75C20.1496 19.34 20.1496 18.67 19.7396 18.26L15.4996 14ZM9.49965 14C7.00965 14 4.99965 11.99 4.99965 9.50002C4.99965 7.01002 7.00965 5.00002 9.49965 5.00002C11.9896 5.00002 13.9996 7.01002 13.9996 9.50002C13.9996 11.99 11.9896 14 9.49965 14Z" fill="#26BCB4"/>
			</svg>
			<input class="input" type="text" name="s" placeholder="Search">
			<input type="hidden" name="post_type" value="post">
		</form>
	</div>

	<?php
	$output = ob_get_clean();
	return $output;
}
