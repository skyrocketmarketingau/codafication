(function($) {

    $(function() {
      const convertImages = (query, callback) => {
        const images = document.querySelectorAll(query);
        images.forEach(image => {
        fetch(image.src)
        .then(res => res.text())
        .then(data => {
          const parser = new DOMParser();
          const svg = parser.parseFromString(data, 'image/svg+xml').querySelector('.elementor-cta__image img');

          if (image.id) svg.id = image.id;
          if (image.className) svg.classList = image.classList;

          image.parentNode.replaceChild(svg, image);
        })
        .then(callback)
        .catch(error => console.error(error))
        });
      }
  });


	$('.clickable').on('click', function(e) {
		e.preventDefault();
		window.location = $(this).find('a').attr('href');
	});

	$('.blog-article').on('click', function(e) {
		e.preventDefault();
		window.location = $(this).find('a').attr('href');
	});

	// Hamburger menu fix
	$(".navbar-toggler").click(function() {
    	$(this).toggleClass("collapsed");
		$(".navbar-nav-wrapper").toggleClass("collapse");
  	});

	$('img.svg').each(function(){
		var $img = $(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		$.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = $(data).find('svg');

			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Replace image with new SVG
			$img.replaceWith($svg);

		}, 'xml');
	});

	$(window).load(function(){


    let mm = new MagnetMouse({
			magnet: {
				element: '.magnet',
				distance: 20, /* Distance (in px) when the magnet effect around element activates */
				enabled: true, /* Enables the magnet effect */
        throttle: 10,
			}
		});

    mm.init();

  });

})(jQuery);
